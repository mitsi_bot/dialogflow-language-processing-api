const Dialogflow = require("dialogflow");
import * as dialogflow from 'dialogflow/protos/protos';
import axios, { AxiosResponse } from 'axios';

import Inputs from "./Inputs";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import IntentDispatcherInput from "mitsi_bot_lib/dist/src/IntentDispatcherInput";
import LanguageProcessingApiInput from "mitsi_bot_lib/dist/src/LanguageProcessingApiInput";
const config = require('config-yml');

// You can find your project ID in your Dialogflow agent settings
const projectId = config.app.dialogflowAgentId;
const sessionId = "123456";
const languageCode = "fr-FR";

const dialogflowCredentials = {
  credentials: {
    private_key: config.app.dialogflowPrivateKey,
    client_email: config.app.dialogflowClientEmail
  }
};
const sessionClient = new Dialogflow.SessionsClient(dialogflowCredentials);
const sessionPath = sessionClient.sessionPath(projectId, sessionId);

export default function processMessage(languageProcessingApiInput: LanguageProcessingApiInput) {
  console.info(new Date(), "[INPUT ]", languageProcessingApiInput.message)
  return compute(languageProcessingApiInput)
    .then((result: ResponseText) => { console.info(new Date(), "[OUTPUT]", result.displayText); return Promise.resolve(result); })
    .catch((error: Error) => { console.error(error); return Promise.reject(error) });
}

function compute(languageProcessingApiInput: LanguageProcessingApiInput): Promise<ResponseText> {
  return new Promise((resolve, reject) => {
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text: languageProcessingApiInput.message,
          languageCode
        }
      }
    };
    sessionClient
      .detectIntent(request)
      .then((responses: dialogflow.google.cloud.dialogflow.v2.DetectIntentResponse[]) => {
        const result = responses[0].queryResult || {};
        if (result.allRequiredParamsPresent &&
          result.action !== Inputs.UNKNOWN &&
          result.action !== Inputs.WELCOME &&
          result.intent && result.intent.displayName) {
          const data = new IntentDispatcherInput(result.intent.displayName, !!languageProcessingApiInput.isAuthenticated, result.parameters);
          axios
            .post(config.webservices.intentDispatcher, data)
            .then((response: AxiosResponse<ResponseText>) => {
              resolve(response.data);
            })
            .catch((err: any) => {
              console.error(err);
              reject(new ResponseText("An error has occured when contacting " + config.webservice.intentDispatcher));
            });
        }
        else {
          resolve(new ResponseText(result.fulfillmentText || "No fullfillment text..."));
        }
      })
      .catch((err: Error) => {
        console.error("ERROR:", err);
        reject(err);
      });
  });
}

module.exports = processMessage;

