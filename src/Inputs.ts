enum Inputs {
  UNKNOWN = "input.unknown",
  WELCOME = "input.welcome"
}

export default Inputs;