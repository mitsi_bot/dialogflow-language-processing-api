import express, { Response, Request } from "express";
import ResponseText from "mitsi_bot_lib/dist/src/ResponseText";
import BaseService from 'mitsi_bot_lib/dist/src/BaseService';
import LanguageProcessingApiInput from "mitsi_bot_lib/dist/src/LanguageProcessingApiInput";

import processMessage from "./ProcessMessage";

class Server extends BaseService {
  onPost(request: Request, response: Response): void {
    processMessage(request.body as LanguageProcessingApiInput)
      .then((result: ResponseText) => {
        response.json(result);
      }).catch((e: any) => {
        console.log("language-processing-api: process message, catch ", e);
        response.json(new ResponseText(e));
      });
  }
}

new Server(express()).startServer();
